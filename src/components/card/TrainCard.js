import React, {useState} from "react";
import StopsList from "../list/StopsList";
import StartDatesList from "../list/StartDatesList";

const TrainCard = (props) => {
    const [stops, setStops] = useState(false);
    const [dates, setDates] = useState(false);

    const onStopsClick = (event) => {
        event.preventDefault();
        setDates(false);
        setStops(true);
    };

    const onDatesClick = (event) => {
        event.preventDefault();
        setStops(false);
        setDates(true);
    };

    const onRollClick = (event) => {
        event.preventDefault();
        setStops(false);
        setDates(false);
    };

    const getButtonRow = (text, handler) => {
        return (
            <div className={"row"} style={{marginTop: '8px'}}>
                <button onClick={handler} className="ui inverted black button"
                        style={{width: '70%', marginLeft: '15%'}}>
                    {text}
                </button>
            </div>
        );
    };

    return (
        <div className={"ui grid"} style={{marginTop: '15px', marginBottom: '15px' ,width: '98%', marginLeft: '1%'}}>
            <div className="four column row">
                <div className={"black column"}>
                    <h3>Pociąg</h3>
                    <h4>{props.train['trainName']}</h4>
                </div>

                <div className={"blue column"}>
                    <h3>Peron </h3>
                    <h4>{props.train['fromPlatform']}</h4>
                </div>

                <div className={"black column"}>
                    <h3>Kierunek </h3>
                    <h4>{props.train['toStation']}</h4>
                </div>

                <div className={"blue column"}>
                    {getButtonRow("Przystanki", onStopsClick)}
                    {getButtonRow("Odjazdy", onDatesClick)}
                </div>
            </div>
            {stops === true ? < StopsList data={props.train['stops']} roll={onRollClick}/> : null}
            {dates === true ? < StartDatesList data={props.train['startDates']} roll={onRollClick}/> : null}
        </div>
    )
};

export default TrainCard;