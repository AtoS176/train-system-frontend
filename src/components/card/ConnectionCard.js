import React, {useState} from "react";
import ConnectionDetails from "../list/ConnectionDetails";
import ReservationForm from "../form/ReservationForm";

const getDate = (data) => {
    const {fragments} = data;
    return fragments[0]['departure'].substring(0, 10);
};

const getTrains = (data) => {
    const {fragments} = data;

    let trainsId = [fragments[0]['id']];
    for (let i = 1; i < fragments.length; i++) {
        if (fragments[i]['trainName'] !== fragments[i - 1]['trainName']) {
            trainsId.push(fragments[i]['id']);
        }
    }
    console.log(trainsId);
    return trainsId;
};

const getStations = (data) => {
    const {fragments} = data;

    let stations = [fragments[0]['startStation']];
    for (let i = 1; i < fragments.length; i++) {
        if (fragments[i]['trainName'] !== fragments[i - 1]['trainName']) {
            stations.push(fragments[i]['startStation']);
        }
    }

    stations.push(fragments.slice(-1)[0]['endStation']);
    console.log(stations);
    return stations;
};

const getTime = (date) => {
    return date.substring(11, 16);
};

const getBasicInfo = (data) => {
    const {fragments} = data;

    const start = {
        name: fragments[0]['startStation'],
        date: getTime(fragments[0]['departure'])
    };

    const end = {
        name: fragments.slice(-1)[0]['endStation'],
        date: getTime(fragments.slice(-1)[0]['arrival'])
    };

    let changes = 0;
    for (let i = 1; i < fragments.length; i++) {
        if (fragments[i]['trainName'] !== fragments[i - 1]['trainName']) {
            changes += 1;
        }
    }

    return {start, changes, end};
};

const ConnectionCard = (props) => {
    const {start, changes, end} = getBasicInfo(props.data);
    const [state, setState] = useState(false);
    const [buy, setBuy] = useState(false);

    const handleClick = () => {
        setBuy(false);
        setState(true);

    };

    const handleBuy = () => {
        setState(false);
        setBuy(true);
    };

    const getStationColumn = (station, color) => {
        return (
            <div className={`four wide ${color} column`}>
                <h3>{station.name}</h3>
                <h4>{station.date}</h4>
            </div>
        );
    };

    const getButtonRow = (text, handler) => {
        return (
            <div className={"row"} style={{marginTop: '8px'}}>
                <button onClick={handler} className="ui inverted black button"
                        style={{width: '70%', marginLeft: '15%'}}>
                    {text}
                </button>
            </div>
        );
    };

    const onClickRoll = () => {
        setState(false);
        setBuy(false);
    };

    return (
        <div className={"ui grid"} style={{marginTop: '30px'}}>
            {getStationColumn(start, "black")}
            {getStationColumn(end, "blue")}
            <div className={"four wide black column"}>
                <p>Liczba przesiadek : {changes}</p>
                <p>Dystants : {props.data['distance']}km</p>
                <p>Koszt : {props.data['price']}zł</p>
            </div>
            <div className={"four wide blue column"}>
                {getButtonRow("Szczegóły", handleClick)}
                {getButtonRow("Rezerwuj", handleBuy)}
            </div>
            {state === true ? < ConnectionDetails data={props.data} roll={onClickRoll}/> : null}
            {buy === true ?
                <ReservationForm
                    when={getDate(props.data)}
                    trains={getTrains(props.data)}
                    stations={getStations(props.data)}
                    onReserve={props.onReserve}
                    onRoll={onClickRoll}
                />
                : null
            }
        </div>
    );
};

export default ConnectionCard;