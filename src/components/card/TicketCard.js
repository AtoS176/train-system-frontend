import React, {useState} from "react";
import MyLoader from "../common/MyLoader";

const fillRow = (arr, typeArr, lastSeat) => {
    arr.push(
        <tr>
            <td data-label="Pociąg" rowSpan={typeArr.length + 1}>{lastSeat['trainName']}</td>
            <td data-label="Odjazd" rowSpan={typeArr.length + 1}>{lastSeat['startTime'].substring(11, 16)}</td>
            <td data-label="Skąd" rowSpan={typeArr.length + 1}>{lastSeat['startStation']}</td>
            <td data-label="Przyjazd" rowSpan={typeArr.length + 1}>{lastSeat['endTime'].substring(11, 16)}</td>
            <td data-label="Dokąd" rowSpan={typeArr.length + 1}>{lastSeat['endStation']}</td>
            <td data-label="Wagon" rowSpan={typeArr.length + 1}>{lastSeat['truckNumber']}</td>
        </tr>
    );
    arr.push(typeArr);
};

const TicketCard = (props) => {
    const [loading, setLoading] = useState(false);

    const onReserveClick = (event) => {
        event.preventDefault();
        setLoading(true);

        fetch('http://localhost:8080/reservation/buy', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(props.ticket)
        }).then(() => {
            setLoading(false);
            props.onReserve();
        })

    };

    const getTicket = () => {
        const seatsInTrains = props.ticket['seatsInTrains'];

        let arr = [];
        let typeArr = [];
        let price = 0;
        let lastSeat = null;

        for (let seat of seatsInTrains) {
            if (lastSeat !== null && lastSeat['trainName'] !== seat['trainName']) {
                fillRow(arr, typeArr, lastSeat);
                typeArr = [];
            }

            typeArr.push(<tr>
                <td data-label="Typ">{seat['seatType']}</td>
                <td data-label="Cena">{seat['price']} zł</td>
            </tr>);

            price += seat['price'];
            lastSeat = seat;
        }

        fillRow(arr, typeArr, lastSeat);
        typeArr.push(<tr className={'negative'}>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td/>
            <td data-label="Cena">{price.toFixed(1)} zł</td>
        </tr>);

        return arr;
    };

    if (loading === true) {
        return <MyLoader context={"Wysyłanie Biletu"}/>
    }

    return (
        <div className={"ui segment"} style={{width: '100%'}}>
            <table className={"ui celled structured table"}>
                <thead>
                <tr>
                    <th>Pociąg</th>
                    <th>Odjazd</th>
                    <th>Skąd</th>
                    <th>Przyjazd</th>
                    <th>Dokąd</th>
                    <th>Wagon</th>
                    <th>Typ</th>
                    <th>Cena</th>
                </tr>
                </thead>
                <tbody>
                {getTicket()}
                </tbody>
            </table>
            <button className={"ui inverted blue button"} type={"submit"} onClick={onReserveClick}>
                Rezerwuj
            </button>
            <button className={"ui inverted blue button"} onClick={props.onRoll}>Anuluj</button>
        </div>
    )
};

export default TicketCard;