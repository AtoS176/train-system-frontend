import React from "react";
import {Container, Dropdown, Segment} from "semantic-ui-react";
import axios from "axios";
import TrainList from "../list/TrainList";

const searchContainerMargin = {
    marginTop: '50px',
    marginBottom: '50px'
};

class SchedulePage extends React.Component {
    state = {
        stations: [],
        station: '',
        trains: []
    };

    componentDidMount() {
        axios.get("http://localhost:8080/stations/getAll")
            .then(response => response.data)
            .then(data => data.map(function (name) {
                return {
                    key: name,
                    text: name,
                    value: name
                };
            }))
            .then(data => this.setState({stations: data}))
    };

    onStationChange = (e, data) => {
        this.setState({station: data.value})
    };

    onFormSubmit = async (event) => {
        event.preventDefault();
        const response = await axios.get("http://localhost:8080/schedule", {
            params: {name: this.state.station}
        });

        this.setState({trains: response.data});
    };

    render() {
        let dataDiv;

        let backgroundImageStyle = {
            backgroundImage: 'url(/images/wall.jpg)',
            backgroundSize: 'cover',
            backgroundPosition: 'center center',
            borderRadius: '0'
        };

        if(this.state.trains.length === 0){
            dataDiv = null;
            backgroundImageStyle.height = '69vh';
        }

        else{
            dataDiv = (
                <Container style={searchContainerMargin}>
                    <TrainList from={this.state.station} trains={this.state.trains}/>
                </Container>
            );
            backgroundImageStyle.height = '30vh';
        }

        return (
            <div>
                <div className={"ui segment"} style={backgroundImageStyle}>
                    <div className={"ui container"} style={searchContainerMargin}>
                        <Segment style={{padding: '2em 2em', width: '50%'}}>
                            <form className={"ui large form "} onSubmit={this.onFormSubmit}>
                                <div className={"field"}>
                                    <Dropdown
                                        placeholder='Wybierz Przystanek'
                                        fluid
                                        selection
                                        options={this.state.stations}
                                        onChange={this.onStationChange}
                                    />
                                </div>
                                <button className={"ui large inverted blue button"} type={"submit"}>Szukaj</button>
                            </form>
                        </Segment>
                    </div>
                </div>
                {dataDiv}
            </div>
        )
    };
}

export default SchedulePage;