import React from "react";
import axios from "axios";
import SearchBar from "../form/SearchBar";
import ConnectionList from "../list/ConnectionList";
import MyLoader from "../common/MyLoader";
import CovidSection from "../section/CovidSection";
import PropSection from "../section/PropSection";

const backgroundImageStyle = {
    backgroundImage: 'url(/images/rail.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundColor: 'black',
    borderRadius: '0'
};

const searchContainerMargin = {
    marginTop: '50px',
    marginBottom: '50px'
};

class MainPage extends React.Component {
    state = {
        connections: [],
        loading: false,
        main: true,
    };

    onSearchSubmit = async ({from, to, when}) => {
        this.setState({loading: true, main: false}, () => {
            axios.get("http://localhost:8080/graph/findConnection", {
                params: {
                    from: from,
                    to: to,
                    when: when
                }
            }).then(response => this.setState({
                loading: false,
                connections: response.data
            }));
        })

    };

    onReserve = () => {
        this.setState({main: true});
    };

    render() {
        let dataDiv;

        if (this.state.loading === true) {
            dataDiv = <MyLoader context={'Wyszukiwanie Połączeń'}/>
        } else if (this.state.main === true) {
            dataDiv = (
                <div className={"ui container"} style={{marginTop: '70px'}}>
                    <CovidSection/>
                    <PropSection/>
                </div>
            );
        } else {
            dataDiv = <ConnectionList connections={this.state.connections} onReserve={this.onReserve}/>
        }

        return (
            <div>
                <div className={"ui segment"} style={backgroundImageStyle}>
                    <div className={"ui container"} style={searchContainerMargin}>
                        <SearchBar onSubmit={this.onSearchSubmit}/>
                    </div>
                </div>
                <div className={"ui container"} style={searchContainerMargin}>
                    {dataDiv}
                </div>
            </div>
        );
    };

}

export default MainPage;