import React from "react";
import Segment from "semantic-ui-react/dist/commonjs/elements/Segment";
import {Container} from "semantic-ui-react";

const backgroundImageStyle = {
    backgroundImage: 'url(/images/cancel.jpg)',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    height: '69vh',
    borderRadius: '0'
};

const searchContainerMargin = {
    marginTop: '40px',
    marginBottom: '50px'
};

class CancelForm extends React.Component {
    state = {
        email: '',
        ticketId: '',
        code: '',
        cancel : null
    };

    onFormSubmit = async(event) => {
        event.preventDefault();
        const url = new URL("http://localhost:8080/reservation/cancel");

        const params = {
            "ticketId": this.state.ticketId,
            "email": this.state.email,
            "code": this.state.code
        };

        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

        await fetch(url)
            .then(response => response.json())
            .then(data => this.setState({
                cancel : data,
                ticketId : '',
                email : '',
                code : ''
            }))
    };

    render() {
        let dataDiv = null;

        if(this.state.cancel === true){
            dataDiv = (
                dataDiv = (
                    <div style={{marginTop: '10px', marginBottom: '10px'}}>
                        <h3 style={{color: 'red', textAlign: 'center'}}>Rezerwacja została anulowana.</h3>
                    </div>
                )
            );
        }

        if(this.state.cancel === false){
            dataDiv = (
                <div style={{marginTop: '10px', marginBottom: '10px'}}>
                    <h3 style={{color: 'red', textAlign: 'center'}}>Dane nie są poprawne lub termin zwrotu minął.</h3>
                </div>
            );
        }

        return (
            <div>
                <Segment style={backgroundImageStyle}>
                    <Container style={searchContainerMargin}>
                        <Segment style={{padding: '2em 2em'}}>
                            {dataDiv}
                            <form onSubmit={this.onFormSubmit} className={"ui form"}>
                                <div className={"field"}>
                                    <div className="ui blue ribbon label" style={{marginLeft : '15px'}}>E-mail</div>
                                    <input
                                        value={this.state.email}
                                        onChange={e => this.setState({email: e.target.value})}
                                    />
                                </div>
                                <div className={"field"}>
                                    <div className="ui blue ribbon label" style={{marginLeft : '15px'}}>Identyfikator Biletu</div>
                                    <input
                                        value={this.state.ticketId}
                                        onChange={e => this.setState({ticketId: e.target.value})}
                                    />
                                </div>
                                <div className={"field"}>
                                    <div className="ui blue ribbon label" style={{marginLeft : '15px'}}>Kod Biletu</div>
                                    <input
                                        value={this.state.code}
                                        onChange={e => this.setState({code: e.target.value})}
                                    />
                                </div>
                                <button className={"ui large inverted blue button"} type={"submit"}>Odwołaj</button>
                            </form>
                        </Segment>
                    </Container>
                </Segment>
            </div>
        )
    }
}

export default CancelForm;