import React from "react";
import {Dropdown, Segment} from "semantic-ui-react";
import {withRouter} from "react-router-dom";
import axios from "axios";

class SearchBar extends React.Component {
    state = {
        from: '',
        to: '',
        when: '',
        stations: [],
        loading: false
    };

    componentDidMount() {
        axios.get("http://localhost:8080/stations/getAll")
            .then(response => response.data)
            .then(data => data.map(function (name) {
                return {
                    key: name,
                    text: name,
                    value: name
                };
            }))
            .then(data => this.setState({stations: data}))
    };

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state);
    };

    onStationFromChange = (e, data) => {
        this.setState({from: data.value});
    };

    onStationToChange = (e, data) => {
        this.setState({to: data.value});
    };

    getStationField = (labelContext, submitType) => {
        return (
            <div className={"field"}>
                <div className="ui blue ribbon label" style={{marginLeft : '15px'}}>{labelContext}</div>
                <Dropdown
                    placeholder='Wybierz Przystanek'
                    fluid
                    selection
                    options={this.state.stations}
                    onChange={submitType}
                />
            </div>
        );
    };

    render() {
        return (
            <Segment style={{padding: '2em 2em', width: '50%'}}>
                <form onSubmit={this.onFormSubmit} className={"ui large form"}>
                    {this.getStationField("Przystnek Początkowy", this.onStationFromChange)}
                    {this.getStationField("Przystnek Końcowy", this.onStationToChange)}
                    <div className={"field"}>
                        <div className="ui blue ribbon label" style={{marginLeft : '15px'}}>Data Podróży</div>
                        <input
                            type={"date"}
                            value={this.state.when}
                            onChange={e => this.setState({when: e.target.value})}
                        />
                    </div>
                    <button className={"ui large inverted blue button"} type={"submit"}>Szukaj</button>
                </form>
            </Segment>
        )
    };

}

export default withRouter(SearchBar);


