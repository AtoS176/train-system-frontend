import React from "react";
import {withRouter} from "react-router-dom";
import TicketCard from "../card/TicketCard";

class ReservationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            trains: props.trains,
            stations: props.stations,
            when: props.when,
            normal: 0,
            senior: 0,
            student: 0,
            invalid: 0,
            email: '',
            ticket: null,
            loading: false
        }
    }

    onSearchSubmit = async (event) => {
        event.preventDefault();

        const that = this;
        let seats = [];

        for (let i = 0; i < this.state.student; i++) seats.push('STUDENT');
        for (let i = 0; i < this.state.normal; i++) seats.push('NORMAL');
        for (let i = 0; i < this.state.invalid; i++) seats.push('INVALID');
        for (let i = 0; i < this.state.senior; i++) seats.push('SENIOR');

        await fetch('http://localhost:8080/seats/prepareTicket', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "trainsId": this.state.trains,
                "seatTypes": seats,
                "stations": this.state.stations,
                "when": this.state.when,
                "email": this.state.email
            })
        }).then(response => response.json())
            .then(json => that.setState({ticket: json}));
    };

    getTypedRow = (typeContext) => {
        let change;

        if (typeContext === 'NORMAL') {
            change = (e) => this.setState({normal: e.target.value});
        } else if (typeContext === 'SENIOR') {
            change = (e) => this.setState({senior: e.target.value});
        } else if (typeContext === 'INVALID') {
            change = (e) => this.setState({invalid: e.target.value});
        } else if (typeContext === 'STUDENT') {
            change = (e) => this.setState({student: e.target.value});
        }

        return (
            <div className={"field"}>
                <div className={"ui blue ribbon label"} style={{marginLeft: '15px'}}>
                    Liczba osób wg taryfy {typeContext}
                </div>
                <input
                    type={"number"}
                    min={0}
                    max={10}
                    defaultValue={0}
                    onChange={change}
                />
            </div>
        )
    };

    checkError = () => { return this.state.ticket['Error'] !== undefined };

    render() {
        let errorDiv = null;

        if (this.state.ticket !== null && this.checkError()) {
            errorDiv = <h3 style={{color: 'red', marginLeft: '10px'}}>{this.state.ticket['Error']}</h3>
        }

        if (this.state.ticket === null || this.checkError()) {
            return (
                <div className={"ui segment"} style={{width: '100%'}}>
                    {errorDiv}
                    <div className="ui message">
                        <div className="header">Taryfy</div>
                        <ul className="list">
                            <li>STUDENT (40% zniżki) : uczniowie do 18 roku życia</li>
                            <li>INVALID (80% zniżki) : osoby niepełnosprawne</li>
                            <li>SENIOR (60% zniżki) : osoby po 65 roku życia</li>
                        </ul>
                    </div>
                    <form onSubmit={this.onSearchSubmit} className={"ui form"} style={{marginLeft: '10px'}}>
                        <div className={"ui grid"}>
                            <div className={"four wide column"}>{this.getTypedRow('NORMAL')}</div>
                            <div className={"four wide column"}>{this.getTypedRow('SENIOR')}</div>
                            <div className={"four wide column"}>{this.getTypedRow('STUDENT')}</div>
                            <div className={"four wide column"}>{this.getTypedRow('INVALID')}</div>
                        </div>
                        <div className={"field"} style={{marginTop: '10px'}}>
                            <div className={"ui blue ribbon label"} style={{marginLeft: '15px'}}>Adres email</div>
                            <input
                                type={"text"}
                                onChange={e => this.setState({email: e.target.value})}
                            />
                        </div>
                        <button className={"ui inverted blue button"} type={"submit"}>Dalej</button>
                        <button className={"ui inverted blue button"} onClick={this.props.onRoll}>Anuluj</button>
                    </form>
                </div>
            )
        }

        return <TicketCard ticket={this.state.ticket} onReserve={this.props.onReserve} onRoll={this.props.onRoll}/>
    };
}

export default withRouter(ReservationForm);
