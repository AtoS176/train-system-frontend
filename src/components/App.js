import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import MyMenu from "./common/MyMenu";
import MyFooter from "./common/MyFooter";
import SchedulePage from "./page/SchedulePage";
import MainPage from "./page/MainPage";
import CancelForm from "./form/CancelForm";

class App extends React.Component {
    render() {
        return (
            <Router>
                <div style={{display: 'flex', minHeight: '100vh', flexDirection: 'column'}}>
                    <MyMenu/>
                    <div style={{flexGrow: '1'}}>
                        <Switch>
                            <Route exact path={'/'}>
                                <MainPage/>
                            </Route>
                            <Route exact path={'/schedule'} component={SchedulePage}/>
                            <Route exact path={'/reservation/cancel'}>
                                    <CancelForm/>
                            </Route>
                        </Switch>
                    </div>
                    <MyFooter/>
                </div>
            </Router>
        );
    }
}

export default App;