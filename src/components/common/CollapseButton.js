import React from "react";

const CollapseButton = (props) => {
    return(
        <div className={"row"} style={{marginTop: '2px'}}>
            <div className={"column blue"}>
                <button className={"fluid ui button blue"} onClick={props.roll}>
                    <h5 style={{fontStyle: 'italic', textAlign: 'center', color: 'white'}}>Zwiń</h5>
                </button>
            </div>
        </div>
    )
};

export default CollapseButton;