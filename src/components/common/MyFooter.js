import React from "react";
import {
    Segment,
    Container,
    Grid,
    Header,
    List
} from "semantic-ui-react";

const MyFooter = () => {
    return (
        <Segment inverted vertical style={{ padding: '5em 0em'}}>
            <Container>
                <Grid divided inverted stackable>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Header inverted as='h3' content='Informacje' />
                            <List link inverted>
                                <List.Item as='a'>Regulamin</List.Item>
                                <List.Item as='a'>Kontakt</List.Item>
                                <List.Item as='a'>Placówki</List.Item>
                                <List.Item as='a'>O nas</List.Item>
                            </List>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Header inverted as='h3' content='Dla pasażera' />
                            <List link inverted>
                                <List.Item as='a'>Stacje kolejowe</List.Item>
                                <List.Item as='a'>Połączenia kolejowe</List.Item>
                                <List.Item as='a'>Kody rabatowe</List.Item>
                            </List>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </Segment>

    )
};

export default MyFooter;