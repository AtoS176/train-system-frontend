import React from "react";
import {Loader, Image} from 'semantic-ui-react'

const MyLoader = (props) => {
    return (
        <div className={"ui segment"} style={{width: '100%'}}>
                <Loader active>
                    {props.context}
                </Loader>
            <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png'/>
        </div>
    )
};

export default MyLoader;
