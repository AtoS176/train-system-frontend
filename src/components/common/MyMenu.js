import React from "react";
import {Menu, Segment, Icon} from "semantic-ui-react";
import { Link } from "react-router-dom";

class MyMenu extends React.Component{
    state = {activeItem: 'strona główna'};

    onItemClick = (e, {name}) => {
        this.setState({activeItem : name});
    };

    getSpecificMenu = (type, link) => {
        return(
            <Menu.Item
                header
                as={Link} to={link}
                name={type}
                active={this.state.activeItem === type}
                onClick={this.onItemClick}
            />
        )
    };

    render() {
        return(
            <Segment inverted vertical>
                <Menu secondary pointing inverted  style={{marginLeft: '10px', fontStyle:'italic'}}>
                    {this.getSpecificMenu('strona główna', '/')}
                    {this.getSpecificMenu('rozkład jazdy', '/schedule')}
                    {this.getSpecificMenu('odwołaj rezerwacje', '/reservation/cancel')}
                    <Menu.Item header position={'right'} style={{marginRight: '50px'}}>
                        Polska Kolej
                        <Icon style={{marginLeft: '20px'}} name={'heart'} color={'red'}/>
                    </Menu.Item>
                </Menu>
            </Segment>
        )
    }
}

export default MyMenu;