import React from "react";
import CollapseButton from "../common/CollapseButton";

const getTime = (date) => {
    return date.substring(11, 16);
};

const calculateWaitingTime = (date1, date2) => {
    return (new Date(date2) - new Date(date1)) / 60000;
};

const ConnectionDetails = (props) => {
    const {fragments} = props.data;
    let details = [];

    for (let i = 0; i < fragments.length; i++) {

        if(i >= 1 && fragments[i]['trainName'] !== fragments[i-1]['trainName']) {
            details.push(
                <div className={"row blue"} style={{marginTop: '2px'}}>
                    <div className={"column"} style={{color: 'white'}}>
                        <h4 style={{fontStyle: 'italic', textAlign: 'center'}}>
                                {
                                    'Przesiadka : czas oczekiwania ' +
                                    calculateWaitingTime(fragments[i-1]['arrival'], fragments[i]['departure']) +
                                    ' minut.'
                                }
                        </h4>
                    </div>
                </div>
            );
        }

        details.push(
            <div className={"row"} style={{marginTop: '2px', backgroundColor : 'white'}}>
                <div className={"column"} style={{color: 'black'}}>
                    <h4>
                        <div style={{marginLeft: '10%', display: 'inline'}}>
                            {fragments[i]['startStation']} {getTime(fragments[i]['departure'])}
                        </div>
                        <div style={{position: "absolute", right: '10%', display: 'inline'}}>
                            {fragments[i]['endStation']} {getTime(fragments[i]['arrival'])}
                        </div>
                    </h4>
                </div>
            </div>
        );
    }

    details.push(<CollapseButton roll={props.roll}/>);

    return details;
};

export default ConnectionDetails;