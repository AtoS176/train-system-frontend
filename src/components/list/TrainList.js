import React from "react";
import TrainCard from "../card/TrainCard";

const getKey = (train) => {
    return train['trainName'] + train['stops'].length;
};

const TrainList = (props) =>{
    const trains = props.trains.map(data => {
        return <TrainCard key={getKey(data)} train={data}/>;
    });

    return <div style={{marginBottom: '30px'}}>{trains}</div>;
};

export default TrainList;