import React from "react";
import CollapseButton from "../common/CollapseButton";

const getPolishDay = (day) => {
    if(day === 'MONDAY') return 'Poniedziałek';
    if(day === 'TUESDAY') return 'Wtorek';
    if(day === 'WEDNESDAY') return 'Środa';
    if(day === 'THURSDAY') return 'Czwartek';
    if(day === 'FRIDAY') return 'Piątek';
    if(day === 'SATURDAY') return 'Sobota';
    if(day === 'SUNDAY') return 'Niedziela';
};

const getMinute = (minute) => {
    if(minute === 0) return '00';
    if(minute < 10) return '0' + minute;
    return minute;
};

const StartDatesList = (props) => {
    const dates = props.data;
    let details = [];

    for (let i = 0; i < dates.length; i++) {
        details.push(
            <div className={"row"} style={{marginTop: '2px', backgroundColor : 'white'}}>
                <div className={"column"} style={{color: 'black'}}>
                    <h4>
                        <div style={{marginLeft: '10%', display: 'inline'}}>
                            {getPolishDay(dates[i]['dayOfWeek'])}
                        </div>
                        <div style={{position: "absolute", right: '10%', display: 'inline'}}>
                            {dates[i]['hour']} :  {getMinute(dates[i]['minute'])}
                        </div>
                    </h4>
                </div>
            </div>
        );
    }

    details.push(<CollapseButton roll={props.roll}/>);

    return details;
};

export default StartDatesList;