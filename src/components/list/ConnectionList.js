import React from "react";
import ConnectionCard from "../card/ConnectionCard";

const backgroundClock = {
    backgroundImage: 'url(/images/clock.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    height: '50vh'
};

const backgroundLost = {
    backgroundImage: 'url(/images/lost.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    height: '50vh'
};

const getCardKey = (data) => {
    const{fragments} = data;

    let key = "";
    for(let i = 0; i < fragments.length; i++){
        key += fragments[i]['id'];
    }
    key += fragments[0]['departure'];
    return key;
};

const ConnectionList = (props) => {

    const getConnectionList = () => {
        if(props.connections.length === 0){
            return <div style={backgroundLost}/>;
        }

        if(props.connections === "Error : wrong date"){
            return <div style={backgroundClock}/>;
        }

        return props.connections.map(data => {
            return <ConnectionCard key={getCardKey(data)} data={data} onReserve={props.onReserve}/>;
        });
    };

    return <div style={{marginBottom: '50px'}}>{getConnectionList()}</div>;
};

export default ConnectionList;

