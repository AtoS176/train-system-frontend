import React from "react";
import CollapseButton from "../common/CollapseButton";

const StopsList = (props) => {
    const empty = 'BRAK PRZYSTANKÓW POŚREDNICH';
    const stops = props.data;
    let details = [];

    const getRow = (stop) => {
        return(
            <div className={"row"} style={{marginTop: '2px', backgroundColor: 'white'}}>
                <div className={"column"} style={{color: 'black'}}>
                    <h4>
                        <div style={{marginLeft: '10%', display: 'inline'}}>
                            {stop}
                        </div>
                    </h4>
                </div>
            </div>
        )
    };

    if(stops.length === 0){
        details.push(getRow(empty))
    }else {
        for (let i = 0; i < stops.length; i++) {
            details.push(getRow(stops[i]));
        }
    }

    details.push(<CollapseButton roll={props.roll}/>);

    return details;
};

export default StopsList;