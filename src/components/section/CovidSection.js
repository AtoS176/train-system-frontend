import React from "react";
import {Card} from "semantic-ui-react";

const CovidSection = () => {
    return (
        <div style={{marginBottom: '70px'}}>
            <h1 style={
                {
                    color: 'white',
                    fontFamily: 'Bookman',
                    textAlign: 'center',
                    marginBottom: '40px'
                }
            }>Bezpieczeństwo w czasach COVID 19</h1>
            <Card.Group textAlign={"center"} style={{fontFamily: 'Bookman'}}>
                <Card
                    centered
                    image='/images/miejsca.jpg'
                    header='Liczba Miejsc'
                    description='W trosce o bezpieczeństwo naszych klientów ograniczyliśmy liczbę miejsc w wagonach o 50%.'
                />
                <Card
                    centered
                    image='/images/higiena.jpg'
                    header='Higiena'
                    description='Przed wejściem do wagonu każdy pasażer jest zobowiązany do dezynfekcji rąk.'
                />
                <Card
                    centered
                    image='/images/posilek.jpg'
                    header='Posiłki'
                    description='Wagony gastronomiczne zostały tymczasowo zawieszone.
                     Pasażer ma tylko możliwość zakupu napoi.'
                />
            </Card.Group>
        </div>
    );
};

export default CovidSection;