import React from "react";
import {Segment, Header, Icon} from "semantic-ui-react";

const PropSection = () => {
    return (
        <div>
            <h1 style={
                {
                    color :'white',
                    fontFamily: 'Bookman',
                    textAlign: 'center',
                    marginBottom: '30px'
                }
            }>Dlaczego warto skorzystać z naszych usług ? </h1>
            <Segment.Group horizontal style={{ padding: '3em 1.5em', marginBottom: '70px', fontFamily: 'Bookman'}}>
                <Segment>
                    <Header>
                        <Icon name='money bill alternate'/>
                        Najniższe ceny
                    </Header>
                    <Segment basic style={{marginTop: '10px'}}>
                        Oferujemy najniższe ceny biletów kolejowych w całym kraju.
                        Dodatkowo oferujemy kody rabatowe dla stałych klientów.
                    </Segment>
                </Segment>
                <Segment>
                    <Header>
                        <Icon name='train'/>
                        Połączenia w całym kraju
                    </Header>
                    <Segment basic style={{marginTop: '10px'}}>
                        Obsługujemy ponad 100 stacji kolejowych na terenie Polski.
                        Oferujemy przejazdy między wszystkimi 16 województwami
                    </Segment>
                </Segment>
                <Segment>
                    <Header>
                        <Icon name='exchange'/>
                        Darmowe zwroty biletów
                    </Header>
                    <Segment basic style={{marginTop: '10px'}}>
                        Każdy bilet można za darmo anulować bez podawania powodu. Należy to zrobić maksymalnie 24h
                        przed odjazdem pociągu.
                    </Segment>
                </Segment>
            </Segment.Group>
        </div>

    )
};

export default PropSection;